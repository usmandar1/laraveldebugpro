<p align="center">
  <img width="130" src="http://dev.laraveldebugpro.com/assets/images/laraveldebugpro-logo.png">
</p>

# Laraveldebugpro
Laravel 5.2+ package for logging errors to [laraveldebugpro.com](https://www.laraveldebugpro.com)

[![Software License](https://poser.pugx.org/laraveldebugpro/laraveldebugpro-laravel/license.svg)](LICENSE.md)
[![Latest Version on Packagist](https://poser.pugx.org/laraveldebugpro/laraveldebugpro-laravel/v/stable.svg)](https://packagist.org/packages/laraveldebugpro/laraveldebugpro-laravel)
[![Total Downloads](https://poser.pugx.org/laraveldebugpro/laraveldebugpro-laravel/d/total.svg)](https://packagist.org/packages/laraveldebugpro/laraveldebugpro-laravel)


## Installation 
You can install the package through Composer.
```bash
composer require laraveldebugpro/laraveldebugpro-laravel
```

Add the LaravelDebugPro service provider and facade in config/app.php:
```php

'providers' => [
    LaravelDebugPro\Provider\ServiceProvider::class,
],

'aliases' => [
    'LaravelDebugger' => \LaravelDebugPro\Facade\LaravelDebugPro::class
],

```
Then publish the config file of the laraveldebugpro using artisan command.
```bash
php artisan vendor:publish --provider="LaravelDebugPro\Provider\ServiceProvider"
```
File (`config/laraveldebugpro.php`) contains all the configuration related to bug reporting.

Note: by default production and local environments will report errors. To modify this edit your laraveldebugpro configuration environment array.

## Environment variables
 Need to define two environment variable and their values.
 
 ```
 LB_PROJECT_ID=
 LB_SECRET=
 ```
Get these variable values under setting page of the project at [laraveldebugpro.com](https://www.laraveldebugpro.com)

# Reporting unhandled exceptions

Add laraveldebugpro reporting to `app/Exceptions/Handler.php` file of laravel.

```php
public function report(Exception $exception)
{
    LaravelDebugPro::report($exception);
    parent::report($exception);
}
``` 

# Reporting handled exceptions

In case of handled exceptions, exceptions can be reported to laraveldebugpro by using `LaravelDebugPro` facade:

```php
try {
    //Code here
 }catch (\Exception $exception){
    LaravelDebugPro::report($exception); 
}
``` 

#Logging specific data

To log specific data to LaravelDebugPro use `log()` method of LaravelDebugPro facade:

```php
$metaData = ['custom_data' => ['x','y','z']]; //Array
LaravelDebugPro::log('Log message here',$metaData);
```

# User feedback

LaravelDebugPro provides the ability to collect feedback from user when an error occurs, 
LaravelDebugPro shows feedback collection page and then feedback is added with the exception report,
to enable this functionality simply need to add `collectFeedback()` method in `render()` method of `app/Exceptions/Handler.php`

```php
public function render($request, Exception $exception)
{
    if (LaravelDebugPro::shouldCollectFeedback($exception) && !$request->wantsJson()) {
        return LaravelDebugPro::collectFeedback();
    }
    return parent::render($request, $exception);
}
```

## License
The laraveldebugpro package is open source software licensed under the [license MIT](http://opensource.org/licenses/MIT)
