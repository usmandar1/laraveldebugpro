<?php

namespace LaravelDebugger\Commands;

use Illuminate\Console\Command;
use LaravelDebugger\Facade\LaravelDebugger;

class LaravelDebugProTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laraveldebugpro:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Laraveldebugpro test command to check configurations, and send test exception';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Testing LaravelDebugger Configurations');

        if (env('LB_PROJECT_ID') && !is_null(env('LB_PROJECT_ID'))){
            $this->info('1. ✓ [LaravelDebugPro] Found project id');
        }else{
            $this->info('1. ✗ [LaravelDebugPro] Could not find your project id, please set this in your .env');
        }

        if (env('LB_SECRET') && !is_null(env('LB_SECRET'))){
            $this->info('2. ✓ [LaravelDebugPro] Found secret key');
        }else{
            $this->info('2. ✗ [LaravelDebugPro] Could not find LaravelDebugger secret, please set this in your .env');
        }

        $requestData = [
            'projectId' => env('LB_PROJECT_ID'),
            'projectSecret' => env('LB_SECRET')
        ];

        if (app('laraveldebugpro') && app('laraveldebugpro')->validateCredentials($requestData)){
            $this->info('3. ✓ [Laraveldebugpro] Validation Success');
        }else{
            $this->info('3. ✗ [Laraveldebugpro] Project id and secret do not match our records');
        }

        try{
            throw new \Exception('Laraveldebugpro Test Exception');
        }catch (\Exception $exception){
            LaravelDebugger::report($exception);
        }

    }

}
