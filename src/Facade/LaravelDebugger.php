<?php


namespace LaravelDebugger\Facade;


use Illuminate\Support\Facades\Facade as FacadeAlias;

class LaravelDebugger extends FacadeAlias
{
    /**
     * @return string
     *
     */
    protected static function getFacadeAccessor()
    {
        return 'laraveldebugpro';
    }
}
