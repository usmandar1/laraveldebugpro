<?php


namespace LaravelDebugger\Provider;


use Illuminate\Support\Facades\Route;
use LaravelDebugger\Commands\LaravelDebugProTestCommand;

trait BootServices
{
    /**
     * List of commands to be registered along with provider
     *
     * @var string[]
     */
    protected $commands = [
        LaravelDebugProTestCommand::class
    ];

    /**
     * BootServices method contains all service that needs to be registered
     */
    private function bootServices()
    {
        $this->publishConfig();
        $this->registerView();
        $this->registerCommands();
        $this->mapLaraBugApiRoutes();
    }

    /**
     * Register view directory with laraveldebugpro namespace
     */
    private function registerView()
    {
        $this->app['view']->addNamespace('laraveldebugpro',__DIR__.'/../../resources/views');
    }

    /**
     * Map api routes directory to enable all api routes for laraveldebugpro
     */
    private function mapLaraBugApiRoutes()
    {
        Route::namespace('\LaravelDebugger\Http\Controllers')
            ->prefix('laraveldebugpro-api')
            ->group(__DIR__ . '/../../routes/api.php');
    }

    /**
     * Publish package config files that contains package configurations
     */
    private function publishConfig()
    {
        if (function_exists('config_path')) {
            $this->publishes([
                __DIR__ . '/../../config/laraveldebugpro.php' => config_path('laraveldebugpro.php'),
            ]);
        }
    }

    /**
     * Register array of commands
     */
    private function registerCommands()
    {
        $this->commands($this->commands);
    }
}
