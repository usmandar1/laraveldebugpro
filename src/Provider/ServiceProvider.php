<?php


namespace LaravelDebugger\Provider;


use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use LaravelDebugger\Client\HttpClient;
use LaravelDebugger\Handler\LaravelDebuggerExceptionHandler;
use LaravelDebugger\Handler\PrepareExceptionData;

class ServiceProvider extends BaseServiceProvider
{
    use BootServices;

    /**
     * boot method of service provider
     */
    public function boot()
    {
        $this->bootServices();
    }

    /**
     * Register method of service provider
     */
    public function register()
    {
        $this->app->singleton('laraveldebugpro',function ($app){
            $this->mergeConfigFrom(__DIR__ . '/../../config/laraveldebugpro.php', 'laraveldebugpro');
            return new LaravelDebuggerExceptionHandler(
                new HttpClient(
                    config('laraveldebugpro.project_id'),
                    config('laraveldebugpro.project_secret')
                ),
                new PrepareExceptionData()
            );
        });
    }


}
