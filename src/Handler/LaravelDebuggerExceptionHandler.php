<?php


namespace LaravelDebugger\Handler;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use LaravelDebugger\Client\HttpClient;

class LaravelDebuggerExceptionHandler
{
    /**
     * @var debugger\src\Client\HttpClient
     */
    private $client;
    /**
     * @var PrepareExceptionData
     */
    private $exceptionData;
    /**
     * @var string
     */
    private $lastExceptionId;

    /**
     * LaravelDebuggerExceptionHandler constructor.
     * @param debugger\src\Client\HttpClient $client
     * @param PrepareExceptionData $exceptionData
     */
    public function __construct(
        HttpClient $client,
        PrepareExceptionData $exceptionData
    )
    {
        $this->client = $client;
        $this->exceptionData = $exceptionData;
    }

    /**
     * Report exception to laraveldebugpro
     *
     * @param \Throwable $exception
     * @return bool
     */
    public function report(\Throwable $exception)
    {
        try {
            if (!$this->checkAppEnvironment()) {
                return false;
            }

            if ($this->skipError(get_class($exception))) {
                return false;
            }

            $data = $this->exceptionData->prepareException($exception);

            $this->client->report($data);

            return true;
        }catch (\Exception $exception){
            Log::info('Lavabug Exception :'.$exception->getMessage());
            return false;
        }
    }

    /**
     * Log details to laraveldebugpro
     *
     * @param $message
     * @param array $meta
     * @return bool
     */
    public function log($message, array $meta = [])
    {
        try {
            $data = $this->exceptionData->prepareLogData($message, $meta);

            $this->client->report($data);

            return true;
        }catch (\Exception $exception){
            Log::info('Lavabug Exception :'.$exception->getMessage());
            return false;
        }
    }

    /**
     * Check if laraveldebugpro environment configurations match with app environment
     *
     * @return bool
     */
    private function checkAppEnvironment()
    {
        if (!config('laraveldebugpro.environment')){
            return false;
        }

        if (is_array(config('laraveldebugpro.environment'))){
            if (count(config('laraveldebugpro.environment')) == 0){
                return false;
            }

            if (in_array(App::environment(),config('laraveldebugpro.environment'))){
                return true;
            }
        }

        return false;
    }

    /**
     * Error dont report, configured in config file
     *
     * @param $class
     * @return bool
     */
    private function skipError($class)
    {
        if (in_array($class,config('laraveldebugpro.skip_errors'))){
            return true;
        }

        return false;
    }

    public function shouldCollectFeedback(\Exception $exception)
    {
        return !$this->skipError(get_class($exception));
    }

    /**
     * Validate env credentials from laraveldebugpro
     *
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(array $credentials)
    {
        return $this->client->validateCredentials($credentials);
    }

    /**
     * Collect error feedback from user
     *
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function collectFeedback()
    {
        if ($this->lastExceptionId) {
            return redirect($this->feedbackUrl().'?exceptionId='.$this->lastExceptionId);
        }

        return redirect('/');
    }

    /**
     * Get feedback url
     *
     * @return mixed
     * @author Syed Faisal <sfkazmi0@gmail.com>
     */
    public function feedbackUrl()
    {
        return URL::to('laraveldebugpro-api/collect/feedback');
    }

    /**
     * Submit user collected feedback
     *
     * @param $data
     * @return bool
     */
    public function submitFeedback($data)
    {
        $this->client->submitFeedback($data);

        return true;
    }

    public function setLastExceptionId(string $exceptionId)
    {
        return $this->lastExceptionId = $exceptionId;
    }

    public function getLastExceptionId()
    {
        return $this->lastExceptionId;
    }

}
